package com.ch4.model.shoppingmall.domain.item;

import com.ch4.model.shoppingmall.domain.category.Category;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ItemCategoryMapper {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "CATEGORY_ID")
  private Category category;

  @ManyToOne
  @JoinColumn(name = "ITEM_ID")
  private Item item;

}
