package com.ch4.model.shoppingmall.domain.delivery;

import com.ch4.model.shoppingmall.domain.order.Order;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Delivery {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @OneToOne(mappedBy = "delivery")
  private Order order;

  private String city;

  private String street;

  private String zipCode;

  @Enumerated(EnumType.STRING)
  private DeliveryStatus status;

  public void setOrder(Order order) {
    this.order = order;
  }

}
