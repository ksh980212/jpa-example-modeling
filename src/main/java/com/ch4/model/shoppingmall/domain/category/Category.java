package com.ch4.model.shoppingmall.domain.category;

import com.ch4.model.shoppingmall.domain.item.ItemCategoryMapper;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Category {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "CATEGORY_ID")
  private Long id;

  private String name;

  @OneToMany(mappedBy = "category")
  private List<ItemCategoryMapper> itemCategoryMapperList;

}
