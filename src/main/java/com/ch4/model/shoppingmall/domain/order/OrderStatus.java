package com.ch4.model.shoppingmall.domain.order;

public enum OrderStatus {
  ORDER, CANCEL
}
