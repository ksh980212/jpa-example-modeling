#자바 ORM 표준 JPA 프로그래밍 쇼핑몰 모델 설계


Member
---------------
id: bigint (pk)

name: varchar

city: varchar

street: varchar

zipcode: varchar


   (Member) 1:n (Orders)

Orders
---------------
id: bigint (pk)

memberId: bigint (fk)

orderDate: date

status: OrderStatus (enum - ORDER, CANCEL)

	(Order) 1 : n (Order_Item)

Order_Item
---------------
id: bigint (pk)

orderId: bigint (fk)

itemId: bigint (fk)

orderPrice: Integer

count: Integer

	(Order_Item) n : 1 (Item)

Item
---------------
id: bigint (pk)

name: varchar

price: Integer

stockQuantity: Integer



현재 문제점
----------------------

테이블 설계가 객체지향 설계가 아닌 테이블 설계에 맞춘 방법이다.

(외래키를 객체에 그대로 가져온 부분)

=> 객체는 외래 키가 아닌 참조를 사용해야함.

따라서, 객체에는 조인이라는 기능이 없어서, Order 객체에서 Member를 탐색할 수 없다. (order.getMember())

----------------
